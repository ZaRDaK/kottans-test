Meteor.publish 'projects', ->
  if @userId
    Meteor.publishWithRelations
      handle: this
      collection: Projects
      filter:
        ownerId: @userId
      mappings: [
        reverse: true
        key: 'projectId'
        collection: Tasks
        options:
          sort:
            order: 1
      ]
  else
    []

Accounts.onCreateUser (options, user) ->
  user.profile = options.profile if options.profile
  Projects.insert
    ownerId: user._id
    title: 'Hi!'
  ,
    (err, projectId) ->
      return if err
      for taskTitle, index in [
        'Bring up the edit form by clicking on corresponding value'
        'Reorder tasks by drag and dropping them'
        'Open this page in two windows and watch magic happen, because #meteorjs'
        'Be sure to drop me back a line'
      ]
        Tasks.insert
          projectId: projectId
          title: taskTitle
          order: index + 1
  user

Meteor.startup ->