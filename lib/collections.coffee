@Projects = new Meteor.Collection 'projects'
@Projects.attachSchema new SimpleSchema
  ownerId:
    type: String
    regEx: SimpleSchema.RegEx.Id
  title:
    type: String
  createdAt:
    type: Date
    autoValue: ->
      return new Date if @isInsert
      return $setOnInsert: new Date if @isUpsert
      @unset()
      undefined

projectFilter = (userId, doc) -> userId and doc.ownerId is userId

@Projects.allow
  insert: projectFilter
  update: projectFilter
  remove: projectFilter
  fetch: ['ownerId']

@Tasks = new Meteor.Collection 'tasks'
@Tasks.attachSchema new SimpleSchema
  projectId:
    type: String
    regEx: SimpleSchema.RegEx.Id
  title:
    type: String
  deadline:
    type: Date
    optional: true
  completed:
    type: Boolean
    defaultValue: false
  order:
    type: Number
    decimal: true

taskFilter = (userId, doc) ->
  return false unless doc.projectId and userId
  project = Projects.findOne doc.projectId
  return false unless project
  project.ownerId is userId

@Tasks.allow
  insert: taskFilter
  update: taskFilter
  remove: taskFilter
  fetch: ['projectId']

Meteor.methods
  removeProject: (projectId) ->
    project = Projects.findOne projectId
    return unless project and @userId and project.ownerId is @userId
    Projects.remove projectId
    Tasks.remove
      projectId: projectId
      