projectsHandle = Meteor.subscribe 'projects'

Template.main.projects = ->
  Projects.find {}, sort: createdAt: -1

Template.project.helpers
  tasks: -> Tasks.find
    projectId: @_id
  ,
    sort:
      order: 1

Meteor.startup ->
  $.fn.editable.defaults = _.extend $.fn.editable.defaults,
    highlight: false
    unsavedclass: null