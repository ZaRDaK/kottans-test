Template.task.helpers
  isChecked: -> @completed

Template.task.events
  'click .task-checkbox': -> Tasks.update @_id, $set: completed: !@completed
  'click .task-remove': (evt) ->
    evt.preventDefault()
    Tasks.remove @_id

Template.taskTitle.rendered = ->
  @autorun =>
    Blaze.getCurrentData()
    $(@find('.task-title')).editable('destroy').editable
      display: ->
      success: (resp, newValue) =>
        Tasks.update @data._id,
          $set:
            title: newValue
        undefined

Template.taskDeadline.deadlineValue = -> if @deadline then moment(@deadline).format 'YYYY-MM-DD HH:mm' else null

Template.taskDeadline.rendered = ->
  @autorun =>
    Blaze.getCurrentData()
    $taskDeadline = $ @find '.task-deadline'
    
    $taskDeadline.editable('destroy').editable
      display: ->
      success: (resp, newValue) =>
        Tasks.update @data._id,
          $set:
            deadline: newValue.toDate()
        $taskDeadline.html newValue.format $taskDeadline.data 'viewformat'
        $taskDeadline.editable 'setValue', newValue
        undefined