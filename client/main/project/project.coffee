Template.projectTitle.rendered = ->
  @autorun =>
    Blaze.getCurrentData()
    $(@find('.project-title')).editable('destroy').editable
      display: ->
      success: (resp, newValue) =>
        Projects.update @data._id,
          $set:
            title: newValue
        undefined

Template.project.rendered = ->
  $tasks = $ @find '.tasks'
  $tasks.sortable
    helper: 'clone'
    update: (evt, ui) =>
      element = ui.item.get 0
      prev = ui.item.prev().get 0
      next = ui.item.next().get 0

      if not prev
        newOrder = UI.getElementData(next).order - 1
      else if not next
        newOrder = UI.getElementData(prev).order + 1
      else
        newOrder = (UI.getElementData(prev).order + UI.getElementData(next).order) / 2

      $tasks.sortable 'cancel'
      Tasks.update UI.getElementData(element)._id,
        $set:
          order: newOrder
      Deps.flush()

addTask = (evt) ->
  $textInput = $(evt.currentTarget).closest('.task-add-container')
      .find '.task-add-text'
  text = $textInput.val()
  if text
    firstTask = Tasks.findOne
      projectId: @_id
    ,
      sort:
        order: 1
    Tasks.insert
      projectId: @_id
      title: text
      order: if firstTask then firstTask.order - 1 else 0
    $textInput.val('')

Template.project.events
  'click .task-add-btn': addTask
  'keypress .task-add-text': (evt) -> addTask.call this, evt if evt.charCode is 13
  'click .project-remove': (evt) ->
    evt.preventDefault()
    Meteor.call 'removeProject', @_id

addProject = (evt) ->
  $textInput = $(evt.currentTarget).closest('.project-add-container')
    .find '.project-add-text'
  text = $textInput.val()
  if text
    Projects.insert
      ownerId: Meteor.userId()
      title: text
    $textInput.val('')

Template.projectAdd.events
  'click .project-add-btn': addProject
  'keypress .project-add-text': (evt) -> addProject.call this, evt if evt.charCode is 13
